![1bkrA_banner.png](https://bitbucket.org/repo/jgpn5Eb/images/444262019-1bkrA_banner.png)

# README
This is a repository for a low-resolution coarse-grained SURPASS model and its applications in molecular modeling of protein structure and dynamics developed by the [Laboratory of Theory of Biopolimers](http://biocomp.chem.uw.edu.pl/). 
The concept of SURPASS representation is very simple and assumes averaging of short secondary structure fragments. The specific interaction model distinguishes the protein-like SURPASS chain from a random polymer. Statistical potentials describe local structural regularities characteristic for most globular proteins.
In order to increase the usability of the model, a non-trivial algorithm for the reconstruction of Cα-trace from SURPASS pseudo atoms was developed. The reconstructed alpha carbon positions reproduce the local geometry of the polypeptide chain and correct spatial orientation of secondary structure elements.
Despite its deep simplification, SURPASS model reproduces reasonable the basic structural properties of proteins. For both small and larger systems, the accuracy of resulting models was quite good for such level of coarse-graining.
The model allows very efficient sampling of the entire conformational space of protein. SURPASS model is a tool that overcomes the limitations of coarse-grained moderate resolution models, which are still too expensive to model efficiently large biomolecular systems and their interactions.
SURPASS model can be easily adapted as an initial step to various multiscale modeling strategies.
 
Detailed instructions and tutorials are provided on [SURPASS WIKI PAGE](https://bitbucket.org/lcbio/surpass/wiki). Any issues should be reported using [SURPASS Issue Tracker](https://bitbucket.org/lcbio/surpass/issues).  
  
Protein folding simulation using SURPASS coarse-grained model can be wached [here](https://www.youtube.com/watch?v=QG7Co4T7oaA).
